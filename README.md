This framework is used to train and evaluate a ANN model using tensorflow and keras.


## Instructions

**For preparing the environment :**

1. Run under a CMSSW release
2. Export the Keras back-end
3. Set PYTHONHOME path

**Commands to run :**

**Step-up CMSSW**
```
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmsrel CMSSW_10_2_15
cd CMSSW_10_2_15/src
cmsenv
export KERAS_BACKEND=tensorflow
export PYTHONHOME=/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/python/2.7.15/
```

**To run the training script :**
```
python train_model_binary.py
```

Two files are produced when running the model training. The **model.h5** file which contains the model weights and architecture and the **variable_norm.csv** file which contains the input feature 
normalization values needed for the evaluation script.

**To run the evaluation script :** 
```
python  evaluate_model.py -i input -o output -j config.json
```

where :


1. **input** Input .root file 
2. **output** Output .root file
3. **config.json** Json file with configs 

The evaluation script produces a root file with the values of the output nodes and any additional branch from the input tree - should configue the **config.json** file accordingly.
