import glob
import os
import json
import sys
import csv
import math as m
import numpy as np
import ROOT as r

r.gROOT.SetBatch(True)

from optparse import OptionParser

def parse_arguments():
    usage = """ 
    usage: %prog [options] Calculates expected pseudosignificance
    """

    parser = OptionParser(usage=usage)

    parser.add_option(  "-s", "--signal",
                        help="""Input .root file with signal events""",
                        dest = "signal",
                        default = "ttbarH125tobbbar_2L.root"
                    )
    parser.add_option(  "-b", "--background",
                        help="""Input .root file with background events""",
                        dest = "background",
                        default = "ttbarsignalplustau_fromDilepton_2018.root"
                    )

    (options, args) = parser.parse_args()
    return options, args

def createDir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def main(options, paths):

    r.gStyle.SetOptStat(0)

    signal_ttH = r.TH1F("ttH_node_s",";ttHbb;Signal / #sqrt{Background}",50,0,1) 
    bkg_ttH = r.TH1F("ttH_node_b",";ttHbb;Signal / #sqrt{Background}",50,0,1)                       

    signal_ttH.Sumw2()
    bkg_ttH.Sumw2()

    f = r.TFile.Open(options.signal)
    for event in f.Get("t1"):
        if(event.N_btags_Medium>=4):   
            for bin in range(1,signal_ttH.GetNbinsX()+1):
                if( event.ttHbb>=(signal_ttH.GetXaxis().GetBinLowEdge(bin)) ):
                    signal_ttH.Fill(signal_ttH.GetBinCenter(bin),1.)  
    
    f = r.TFile.Open(options.background)
    for event in f.Get("t1"):
        if(event.N_btags_Medium>=4):
            for bin in range(1,bkg_ttH.GetNbinsX()+1):
                if( event.ttHbb>=(bkg_ttH.GetXaxis().GetBinLowEdge(bin)) ):
                    bkg_ttH.Fill(bkg_ttH.GetBinCenter(bin),1.)

    for bin in range(1,bkg_ttH.GetNbinsX()+1):
        if(bkg_ttH.GetBinContent(bin)>0):
            signal_ttH.SetBinContent(bin,signal_ttH.GetBinContent(bin)/m.sqrt(bkg_ttH.GetBinContent(bin)))   

    print('No cut value : ', signal_ttH.GetBinContent(1) )
    print('Maximum value : ', signal_ttH.GetBinContent(signal_ttH.GetMaximumBin()), 'bin value : ', signal_ttH.GetBinCenter(signal_ttH.GetMaximumBin() ))


    paveCMS = r.TPaveText(0.12,0.93,0.92,0.96,"NDC");
    paveCMS.AddText("#bf{CMS Simulation (#it{Work In Progress)}}  (13 TeV)")
    paveCMS.SetFillColor(0)
    paveCMS.SetBorderSize(0)
    paveCMS.SetTextSize(0.04)
    paveCMS.SetTextFont(42)

    c = r.TCanvas("c", "canvas", 900, 600)
    c.cd()
    signal_ttH.SetLineColor(r.kRed)
    signal_ttH.SetLineWidth(2)
    signal_ttH.Draw("HIST")
    paveCMS.Draw("same")
    c.SaveAs('SoverB.png')

if __name__ == '__main__':
    options, paths = parse_arguments()
    main(options = options, paths = paths)


