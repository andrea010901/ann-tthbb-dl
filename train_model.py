#!/usr/bin/env python

from __future__ import print_function

import time
start_time = time.time()

from keras.utils import np_utils
from keras.utils import to_categorical
import tensorflow as tf
from keras.models import load_model
from tensorflow import keras
from keras import layers
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras import regularizers
from keras.wrappers.scikit_learn import KerasClassifier
from keras.callbacks import EarlyStopping
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import auc
from sklearn.svm import SVC
from matplotlib import pyplot
import ROOT
import numpy as np
import pandas as pd
import uproot as uprt
import seaborn as sns
import h5py
import csv

from my_functions import compare_train_test
from my_functions import selection_criteria
from my_functions import plot_input_features
from my_functions import AUC_ROC

def prepare_data():

    # Get data from root files
    samples = ['ttbarH125tobbbar_2L_allVars_2018','ttbarsignalplustau_fromDilepton_allVars_2018_tt-B','ttbarsignalplustau_fromDilepton_allVars_2018_tt-cc','ttbarsignalplustau_fromDilepton_allVars_2018_tt-lf']       
    DataFrames = {} # define empty dictionary to hold dataframes
    Selection = {}    

    Selection_inputs = ["MEM","N_btags_Medium","njets","mbb"]
    #ML_inputs = ["MEM","blr","N_btags_Loose","mass_tag_tag_min_deltaR","mass_tag_tag_max_mass","mass_jet_tag_tag_max_pT",
    #    "pT_tag_tag_min_deltaR","avgDeltaR_tag_tag","avgDeltaR_jet_jet","maxDeltaEta_tag_tag","maxDeltaEta_jet_jet",
    #    "H2_tag","H4_tag","R2_jet","centrality_tags","sphericity_tag","jet4_btag","pT_jet_tag_min_deltaR",
    #    "multiplicity_higgsLikeDijet15"] # list of features for ML model 

    ML_inputs = ["mbb","N_btags_Loose","N_btags_Medium","pT_jet_jet_min_deltaR","multiplicity_higgsLikeDijet15","minDeltaR_jet_jet","mass_higgsLikeDijet",
                 "H4_jet","HT_jets","aplanarity_jet","C_jet","D_jet","mass_jet_jet_jet_max_pT","maxDeltaEta_jet_jet","jet2_pt"] 

    for s in samples: # loop over samples
        file = uprt.open("/afs/cern.ch/work/c/ckoraka/produceVarTrees/"+s+".root")
        tree = file["t1"]
	DataFrames[s] = tree.pandas.df(ML_inputs)
        Selection[s] = tree.pandas.df(Selection_inputs)
        DataFrames[s] = DataFrames[s][ np.vectorize(selection_criteria)(Selection[s].MEM,Selection[s].N_btags_Medium,Selection[s].njets,Selection[s].mbb) ]
        DataFrames[s] = DataFrames[s].iloc[0:12000] # first rows of dataframe

    all_MC = [] # define empty list that will contain all features for the MC
    all_y = [] # define empty list that will contain labels whether an event in signal or background

    for s in samples: # loop over the different samples
        if s!='data': # only MC should pass this
            all_MC.append(DataFrames[s][ML_inputs]) # append the MC dataframe to the list containing all MC features
            if 'ttbarH125tobbbar' in s: # only signal MC should pass this
                all_y.append(np.zeros(DataFrames[s].shape[0])) # signal events are labelled with 0
            elif 'tt-B' in s:
                all_y.append(np.full(DataFrames[s].shape[0],1)) # ttB background events are labelled 1
            elif 'tt-cc' in s:
                all_y.append(np.full(DataFrames[s].shape[0],2)) # ttcc background events are labelled 2
            else:
                all_y.append(np.full(DataFrames[s].shape[0],3)) # ttlf background events are labelled 3

    X = np.concatenate(all_MC) # concatenate the list of MC dataframes into a single 2D array of features, called X
    y = np.concatenate(all_y) # concatenate the list of lables into a single 1D array of labels, called y

    #for x,var in enumerate(ML_inputs):
    #    plot_input_features(X, y, x,var)
    
    # make train and test sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=492 ) # set the random seed for reproducibility

    #AUC_ROC(X_train, y_train, X_test, y_test)  # Plot ROC curve One-vs-Rest

    scaler = StandardScaler() # initialise StandardScaler
    scaler.fit(X_train) # Fit only to the training data
    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    X_scaled = scaler.transform(X)

    # Save .csv with normalizations
    mean = scaler.mean_
    std  = scaler.scale_
    with open('variable_norm.csv', mode='w') as norm_file:
        headerList = ['', 'mu', 'std']
        norm_writer = csv.DictWriter(norm_file,delimiter=',',fieldnames=headerList) 
        norm_writer.writeheader()
        for x,var in enumerate(ML_inputs): 
            print(var,mean[x],std[x])
            norm_writer.writerow({'': var, 'mu': mean[x], 'std': std[x]})

    X_valid_scaled, X_train_nn_scaled = X_train_scaled[:1000], X_train_scaled[1000:] # first 1000 events for validation
    y_valid, y_train_nn = y_train[:1000], y_train[1000:] # first 1000 events for validation

    y_valid_b = to_categorical(y_valid)
    y_test_b = to_categorical(y_test)
    y_train_nn_b = to_categorical(y_train_nn)

    print(y_train_nn)
    print(y_train_nn_b)
    print('Training set', X_train_nn_scaled.shape, y_train_nn_b.shape)
    print('Validation set', X_valid_scaled.shape, y_valid_b.shape)
    print('Test set', X_test_scaled.shape, y_test_b.shape)
    
    print('Input feature correlation')
    fig = pyplot.figure(figsize=(20, 16))
    corrMatrix = DataFrames['ttbarH125tobbbar_2L_allVars_2018'].corr()
    print(DataFrames['ttbarH125tobbbar_2L_allVars_2018'].corr()) #Pearson
    sns.heatmap(corrMatrix, annot=True, cmap=pyplot.cm.Blues)
    pyplot.savefig('correlation.png')

    return X_train_nn_scaled, y_train_nn_b, X_test_scaled, y_test_b, X_valid_scaled, y_valid_b


def nn_model():

    # create model
    model = Sequential()
    model.add(Dense(249, input_dim=12,kernel_regularizer=regularizers.l1_l2(l1=1e-5,l2=7*1e-4)))
    model.add(layers.LeakyReLU())
    model.add(layers.Dropout(0.09))
    model.add(Dense(249,kernel_regularizer=regularizers.l1_l2(l1=1e-5,l2=7*1e-4)))
    model.add(layers.LeakyReLU())
    model.add(layers.Dropout(0.09))
    model.add(Dense(249,kernel_regularizer=regularizers.l1_l2(l1=1e-5,l2=7*1e-4)))
    model.add(layers.LeakyReLU())
    model.add(layers.Dropout(0.09))
    model.add(Dense(4, activation='softmax'))

    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer=optimizers.Adagrad(lr=0.001,epsilon=1e-07), metrics=['accuracy'])

    return model

def train_model(X_train, y_train, X_test, y_test, X_val, y_val):

    # fetch cnn model
    model = nn_model()

    # simple early stopping
    es = EarlyStopping(monitor='val_loss', mode='min', patience=50, verbose=1)

    xsection_signal=31.4 #fb
    xsection_bkg=88341.9 #fb
    N_signal=9572600.
    N_bkg=64310000.
    Lumi=1000000. #fb^-1
    N_ttHbb=459893.
    r_ttbb=27813.
    r_ttcc=16186.
    r_ttlf=12428.

    weight_ttHbb= xsection_signal*Lumi/N_signal
    weight_ttbar= xsection_bkg*Lumi/N_bkg

    # set sample weights
    sample_weight = np.ones(shape=(len(y_train),))
    sample_weight[y_train[:,0]==1] = 1.
    sample_weight[y_train[:,1]==1] = 1.
    sample_weight[y_train[:,2]==1] = 1.
    sample_weight[y_train[:,3]==1] = 1.

    # fit model
    history = model.fit(X_train, y_train, epochs=600, batch_size=2048, validation_data=(X_val, y_val), verbose=1, callbacks=[es])
     
    decisions_tf = model.predict_proba(X_train[y_train[:,0]>-1])[:, 0]
    y_ = (y_train[:,0]).flatten()
    print(y_train)
    print(y_,len(y_))
    print(decisions_tf,len(decisions_tf))

    fpr_tf, tpr_tf, thresholds_tf = roc_curve(y_, decisions_tf)
    fig = pyplot.figure(figsize=(16, 16))
    pyplot.plot(tpr_tf, 1-fpr_tf, linestyle='--',linewidth=4,color='blue', label='Class ttHbb vs Rest - AUC')
    pyplot.title('Multiclass ROC curve')
    pyplot.xlabel('Signal efficiency')
    pyplot.ylabel('Background Rejection')
    pyplot.grid(True)
    pyplot.legend(loc='best')
    pyplot.savefig('Multiclass-ROC.png',dpi=300);
    pyplot.close(fig)
	
    # plot train-test comparisons
    compare_train_test(model,X_train,y_train,X_test,y_test,'TensorFlow Neural Network output : ttHbb node',0)
    compare_train_test(model,X_train,y_train,X_test,y_test,'TensorFlow Neural Network output : ttbb node ',1) 
    compare_train_test(model,X_train,y_train,X_test,y_test,'TensorFlow Neural Network output : ttcc node ',2) 
    compare_train_test(model,X_train,y_train,X_test,y_test,'TensorFlow Neural Network output : ttlf node ',3) 

    # evaluate model
    loss, acc = model.evaluate(X_test, y_test, verbose=1)
    print('Test loss: {:.4f}'.format(loss))
    print('Test accuracy: {:.4f}'.format(acc))

    # confusion matrix
    y_pred = model.predict(X_test)
    Y_pred = np.argmax(y_pred, 1) # returns indices of the maximum values along an axis (array,axis)
    Y_test = np.argmax(y_test, 1)
    mat = confusion_matrix(Y_test, Y_pred)
    classes = [0, 1, 2, 3]
    con_mat_norm = np.around(mat.astype('float') / mat.sum(axis=1)[:, np.newaxis], decimals=2)
    con_mat_df = pd.DataFrame(con_mat_norm, index=classes, columns=classes)

    # plot confusion matrix
    fig1 = pyplot.figure(figsize=(10, 10))
    sns.heatmap(con_mat_df, annot=True, cmap=pyplot.cm.Blues)
    pyplot.tight_layout()
    pyplot.ylabel('True label')
    pyplot.xlabel('Predicted label')
    #pyplot.show()
    pyplot.savefig('confusion_matrix.png')

    # save trained model
    model.save('model.h5')

    return acc, history


def plot_model(history):
    # plot entropy loss
    pyplot.subplot(2, 1, 1)
    pyplot.title('Entropy Loss')
    pyplot.plot(history[1].history['loss'], color='blue', label='train')
    pyplot.plot(history[1].history['val_loss'], color='red', label='test')

    # plot accuracy
    pyplot.subplot(2, 1, 2)
    pyplot.title('Accuracy')
    pyplot.plot(history[1].history['acc'], color='blue', label='train')
    pyplot.plot(history[1].history['val_acc'], color='red', label='test')
    pyplot.savefig('loss_accuraccy.png')


def main():

    # 1 load train dataset
    X_train, y_train, X_test, y_test, X_val, y_val = prepare_data()

    print('Training set', X_train.shape, y_train.shape)
    print('Validation set', X_val.shape, y_val.shape)
    print('Test set', X_test.shape, y_test.shape)

    # Different way to train model
    #estimator = KerasClassifier(build_fn=nn_model, epochs=300, batch_size=4000, verbose=1)
    #kfold = KFold(n_splits=10, shuffle=True)
    #results = cross_val_score(estimator, X_train, y_train, cv=kfold)
    #print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))

    # 2 train model
    #history = train_model(X_train, y_train, X_test, y_test, X_val, y_val)

    # 3 plot model
    #plot_model(history)

    # 4 for some checks - remove
    #model = load_model("model.h5")
    #model.summary()
    #compare_train_test(model,X_train,y_train,X_test,y_test,'TensorFlow Neural Network output')
    #print( model.predict( prepare_data(options.input).values ) )
    #decisions_tf = history.predict_proba(X_test_scaled)[:,1] # get the decisions of the TensorFlow neural network
    #fpr_tf, tpr_tf, thresholds_tf = roc_curve(y_test, decisions_tf)

    print(('\033[1m'+'> Time Elapsed = {:.3f} sec'+'\033[0m').format((time.time()-start_time)))

if __name__ == "__main__":
    main()

